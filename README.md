# HomeAway 2.0

## Table of Contents

* [About the Game](#about-the-game)
  * [Built With](#built-with)
* [State of the game and version](#version-of-the-game)
  * [Actuality](#actualoity)
  * [Version](#version)
* [Usage](#usage)  
  * [Screenshots](#screenshots)
* [Creators](#creators)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)




## About The Game

![The main menu of the game](GitLab_images/main_menu.png "the main menu")

In HomeAway 2.0, you will live a complete adventure composed of storytelling, mini-games and adaptative behaviors. You are a farmer and live a happy and relaxing in the countryside of a fantasy world. Everythings is going fine until a horde of barbars comes and destroys your farm and everything you had. You have to leave your home in a hurry and can only choose 3 items out of 6 to flee the horde. Which ones will you choose ? This choice will chape your entier adventure and lead you to different ways of completing the different mini-games and will entierly change the dialogs of the adventure. You will have different endings giving what choices you made at the begining.

The game has very interesting characters, wacky humour, 3 mini games and more than 5 different endings. The artistic style of the game is cartoony and we have provided 7 different dialogs with unique characters to complete the overall story of the game.

HomeAway is a 2D video game made during the 2019 Global Game Jam in Paris. The goal was to create a video game in two days with a specific theme given to all participants. Our team was composed of 2 game programmer, 2 artists, 1 game designer and 1 sound designer. We are all passionate about video games or are currently studying on this domain. We worked together to produce this fun video game. The theme of the 2019 edition was "Home"

The original game (1.0) was created during the GGJ event but wasn't completely finished and had serious problems (no sound, bugs, incompletes mini-games) but was fun overall. I decided to re-create it on my own from scratch and produce a problem-free game that can be enjoyed entierly. It is a shame that all this work cannot be seen and played (only 40% of the game was playable for the 1.0). The 2.0 will be final complete version of the game with all the dialogs, all the mini-games, all the endings and without any bugs. I am currently working on it !

### Built With

* [Unity](https://unity.com/)
* [PhotoShop](https://unity.com/)
* [FMOD](https://www.fmod.com/)
* A lot of hard work !



## Version of the game

### Actuality

The game is currently being re-created from scratch by me. I am currently working on it.

### Version

* 1.0 : 2019 during the Global Game Jam
* 2.0 : Currently being developed

## Usage

### Screenshots

That is how the main menu looks like :

![The main menu of the game](GitLab_images/main_menu.png "the main menu")

You have to choose three items per types to go on you adventure :

![Items](GitLab_images/items.png "Items")

This is the first challenge you have to undergo, escape from the barbars with your first item choice (the chicken or the donkey) :

![Mini-game 1](GitLab_images/game1.png "Mini-game 1")

You can loose at anytime :

![You loose](GitLab_images/loose.png "You loose")

New screenshots will be uploaded as the development progresses... Be patient :D

## Creator

Of the 2.0 version :

* Théo Ducatez (Computer engineering student)

Of the 1.0 version :

* Théo Ducatez (Computer engineering student)
* Alexis Facques (Video game Artist student)
* Robin Sulmona (Video game Artist student)
* Eddy Briand (Game design student)
* Melchior (Game design student)
* Nourdine (Game programmer student)

## Contact

Théo Ducatez - [theo-ducatez](https://www.linkedin.com/in/theo-ducatez/) - theoducatez05@gmail.com

Project Link: [https://gitlab.com/theoducatez05/wasted](https://gitlab.com/theoducatez05/wasted)



## Acknowledgements
* [Global Game Jam](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Unity](https://unity.com/)
* [ISART Paris](https://www.isart.com/)
