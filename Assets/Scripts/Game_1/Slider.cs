﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slider : MonoBehaviour
{
    [SerializeField]
    protected float speed;

    protected Vector2 initialPosition;

    private float _timer = 0f;

    // Start is called before the first frame update
    protected void Start()
    {
        this.initialPosition = this.transform.position;   
    }

    // Update is called once per frame
    protected void Update()
    {
        _timer += Time.deltaTime;
        this.transform.position = this.initialPosition + Vector2.left * _timer * this.speed;
    }
}
