﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSlider : MonoBehaviour
{
    [SerializeField]
    float speed;

    private Vector2 childInitialPosition;

    [SerializeField]
    private Camera cam;
    private float leftBottomPointX;

    [SerializeField]
    GameObject back_1;
    [SerializeField]
    GameObject back_2;

    private GameObject activeBack;
    private GameObject otherBack;

    private float _timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        this.activeBack = this.back_1;
        this.otherBack = this.back_2;
        this.childInitialPosition = activeBack.transform.position;

        leftBottomPointX = cam.ScreenToWorldPoint(new Vector2(0, 0)).x;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (this.activeBack.GetComponent<SpriteRenderer>().transform.position.x + (this.activeBack.GetComponent<SpriteRenderer>().size.x / 2) < leftBottomPointX)
        {
            this.activeBack.transform.localPosition = new Vector2(this.otherBack.transform.localPosition.x + this.otherBack.GetComponent<SpriteRenderer>().size.x,0);

            if (this.activeBack == this.back_1)
            {
                this.activeBack = this.back_2;
                this.otherBack = this.back_1;
            }
            else
            {
                this.activeBack = this.back_1;
                this.otherBack = this.back_2;
            }
        }

        this.transform.position = childInitialPosition + Vector2.left * _timer * this.speed;
        
    }
}
