﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Game_1 : MonoBehaviour
{
    //objects
    [SerializeField]
    private Level1_Manager miniGameManager;
    [SerializeField]
    private Image lifeBarFG;

    //state
    [SerializeField]
    private FirstItem mode;

    private float horizontalInput = 0f;
    private bool falling;
    private bool facingRight = true;
    private bool jumpRequest;
    private bool invicible = false;


    //parameters
    [SerializeField]
    private float playerSpeed;
    private float jumpsLeft;
    [SerializeField]
    private float maxJumps;
    [SerializeField]
    private float jumpVelocity;
    private float chickenJumpVelocitySave;
    [SerializeField]
    private float fallMultiplier;
    [SerializeField]
    private int maxHealth;
    [SerializeField]
    private int health;
    [SerializeField]
    private float invincibilityTimer;
    [SerializeField]
    private float invincibilityDuration;
    [SerializeField]
    private float reboundVelocity;


    //Components
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D circleCollider;
    private Rigidbody2D playerRigidbody;

    private float Alpha
    {
        get
        {
            return alpha;
        }

        set
        {
            alpha = value;
            if (spriteRenderer != null)
                SetAlpha(spriteRenderer, alpha);
        }
    }
    private float alpha;

    // Start is called before the first frame update
    void Start()
    {
        //Component getter
        this.spriteRenderer = this.GetComponentInChildren<SpriteRenderer>();
        this.circleCollider = this.GetComponent<CircleCollider2D>();
        this.playerRigidbody = this.GetComponent<Rigidbody2D>();

        this.jumpsLeft = this.maxJumps;
        this.health = this.maxHealth;

        if (this.miniGameManager.getFirstItem() == FirstItem.Chicken)
        {
            this.mode = FirstItem.Chicken;
            this.jumpVelocity *= 0.60f;
            this.spriteRenderer.sprite = this.miniGameManager.chickenSprite_down;
        }
        else
        {
            this.mode = FirstItem.Donkey;
            this.spriteRenderer.sprite = this.miniGameManager.donkeySprite_down;
        }

        this.chickenJumpVelocitySave = this.jumpVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        this.horizontalInput = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            if (this.mode == FirstItem.Chicken)
            {
                this.jumpRequest = true;
            }
        }

        this.fallAnimation(this.mode, this.falling);

        //flip
        if (this.horizontalInput < 0.0f && this.facingRight == true)
        {
            this.flipPlayer();
        }
        else if (this.horizontalInput > 0.0f && this.facingRight == false)
        {
            this.flipPlayer();
        }
    }

    private void FixedUpdate()
    {
        //falling
        if (this.playerRigidbody.velocity.y < 0)
        {
            this.falling = true;
            this.playerRigidbody.gravityScale = this.fallMultiplier;
        }
        else
        {
            this.falling = false;
            this.playerRigidbody.gravityScale = 3.5f;
        }

        this.move();
        this.jump();
    }

    private void jump()
    {
        if (this.jumpRequest)
        {
            if (this.jumpsLeft > 0)
            {
                this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, Vector2.up.y * this.jumpVelocity);
                this.jumpsLeft -= 1;
                this.jumpVelocity *= 0.70f;
                this.jumpRequest = false;
            }
        }
    }

    private void jumpAuto()
    {
        this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, Vector2.up.y * this.jumpVelocity);
    }

    private void fallAnimation(FirstItem mode, bool falling)
    {
        if (falling)
        {
            if (mode == FirstItem.Chicken)
            {
                this.spriteRenderer.sprite = this.miniGameManager.chickenSprite_down;
            }
            else
            {
                this.spriteRenderer.sprite = this.miniGameManager.donkeySprite_down;
            }
        }
        else
        {
            if (mode == FirstItem.Chicken)
            {
                this.spriteRenderer.sprite = this.miniGameManager.chickenSprite_up;
            }
            else
            {
                this.spriteRenderer.sprite = this.miniGameManager.donkeySprite_up;
            }
        }
    }

    private void move()
    {
        Vector2 HorizontalDirection = new Vector2(this.horizontalInput * this.playerSpeed, this.playerRigidbody.velocity.y);
        this.playerRigidbody.velocity = HorizontalDirection;  
    }

    private void flipPlayer()
    {
        this.facingRight = !this.facingRight;
        Vector2 localScale = this.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("floor"))
        {
            this.jumpsLeft = this.maxJumps;

            if (this.mode == FirstItem.Chicken)
            {
                this.jumpVelocity = this.chickenJumpVelocitySave;
            }

            this.jumpAuto();
        }
        else if (col.gameObject.tag.Equals("spike"))
        {
            if (!this.invicible)
            {
                this.health -= 1;
                this.lifeBarFG.fillAmount -= (1f/this.maxHealth);

                if (this.health == 0)
                {
                    miniGameManager.PlayerDead();
                }

                this.TriggerInvincibility();
                this.damageRebound(col);
            }
        }
        else if (col.gameObject.tag.Equals("floorEdge"))
        {
            this.playerRigidbody.velocity = new Vector2(0, Vector2.up.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("DeathZone"))
        {
            miniGameManager.PlayerDead();
        }
        else if (collision.gameObject.name.Equals("WinZone"))
        {
            miniGameManager.gameWin();
        }
    }

    private void damageRebound(Collision2D col)
    {
        this.playerRigidbody.velocity = new Vector2(this.playerRigidbody.velocity.x, col.GetContact(0).normal.y + this.reboundVelocity);
    }

    //Invincibility :
    private void TriggerInvincibility()
    {
        this.invicible = true;
        this.invincibilityTimer = invincibilityDuration;
        StartCoroutine("InvicibilityTimerCoroutine");
        StartCoroutine("BlinkCoroutine");
    }

    private IEnumerator InvicibilityTimerCoroutine()
    {
        while (invincibilityTimer >= 0)
        {
            invincibilityTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        invicible = false;

        yield return null;
    }

    private IEnumerator BlinkCoroutine()
    {
        while (invicible)
        {
            Alpha = (Alpha == 1) ? 0 : 1;
            yield return new WaitForSeconds(0.1f);
        }

        Alpha = 1;

        yield return null;
    }

    private static void SetAlpha(SpriteRenderer renderer, float alpha)
    {

        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);

    }
}
