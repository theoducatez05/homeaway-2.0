﻿using UnityEngine;

public class Collectible_item : MonoBehaviour,IDescribable
{
    public int itemtype;
    public int id;
    public bool selected;
    public Collectible_item otherItem;

    [SerializeField]
    private string title;

    [SerializeField]
    [TextArea]
    private string Description;

    public Level0_manager miniGameManager;

    private void Awake()
    {
        this.miniGameManager = FindObjectOfType<Level0_manager>(); // add cast
    }

    private void OnMouseDown()
    {
        take();
        miniGameManager.reloadInventory();
        this.miniGameManager.HideToolTip();
    }

    private void OnMouseEnter()
    {
        this.miniGameManager.ShowToolTip(this.transform.position,this);
    }

    private void OnMouseExit()
    {
        this.miniGameManager.HideToolTip();
    }

    public void take()
    {
        if (otherItem != null)
        {
           otherItem.disapear();
        }
        gameObject.SetActive(false);
        miniGameManager.addItem(this);
    }

    public void disapear()
    {
        //make disapear the object
        SpriteRenderer renderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        GetComponent<Collider2D>().enabled = false;
        this.gameObject.GetComponentInChildren<Animator>().enabled = false;
        renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0.5f);
    }

    public string GetDescription()
    {
        return this.title + "\n" + this.Description;
    }
}
