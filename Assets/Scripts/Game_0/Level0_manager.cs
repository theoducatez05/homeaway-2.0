﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Level0_manager : MiniGameManager
{
    private int collectedItem = 0;
    private GameManager gameManager;

    public GameObject tootlip;

    // Start is called before the first frame update
    void Start()
    {
        this.gameManager = FindObjectOfType<GameManager>();
    }

    private void Awake()
    {
        GameObject check = GameObject.Find("__app");
        if (check == null)
        { UnityEngine.SceneManagement.SceneManager.LoadScene("_preload"); }
    }

    public void reloadInventory()
    {
        FindObjectOfType<UI_inventory>().reloadUI();
    }

    public void addItem(Collectible_item item)
    {
        if (this.gameManager != null)
        {
            switch (item.itemtype)
            {
                case 2:
                    if (item.id == 1)
                        this.gameManager.GetInventory().thirdItem = ThirdItem.Chestnut;
                    else
                        this.gameManager.GetInventory().thirdItem = ThirdItem.Blower;
                    break;
                case 0:
                    if (item.id == 1)
                        this.gameManager.GetInventory().firstItem = FirstItem.Donkey;
                    else
                        this.gameManager.GetInventory().firstItem = FirstItem.Chicken;
                    break;

                case 1:
                    if (item.id == 1)
                        this.gameManager.GetInventory().secondItem = SecondItem.Beam;
                    else
                        this.gameManager.GetInventory().secondItem = SecondItem.Mandolin;
                    break;
            }
            this.collectedItem += 1;

            if (this.collectedItem == 3)
            {
                Debug.Log("Next level !");
                gameManager.loadNextScene();
            }
        }
    }

    public void ShowToolTip(Vector2 position, IDescribable description)
    {
        this.tootlip.SetActive(true);
        this.tootlip.transform.position = position;
        this.tootlip.GetComponentInChildren<Text>().text = description.GetDescription();
    }

    public void HideToolTip()
    {
        this.tootlip.SetActive(false);
    }
}
