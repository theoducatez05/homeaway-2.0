﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ui_item : MonoBehaviour, IComparable
{
    public Sprite image1;
    public Sprite image2;

    public int CompareTo(object obj)
    {
        return this.name.CompareTo(((ui_item)obj).name);
    }
}
