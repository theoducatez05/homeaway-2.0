﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UI_inventory : MonoBehaviour
{
    GameManager gameManager;

    [SerializeField]
    ui_item[] items = new ui_item[3];

    private FirstItem first;
    private SecondItem second;
    private ThirdItem third;

    private void Awake()
    {
        this.gameManager = FindObjectOfType<GameManager>();
        Array.Sort(items);
    }

    public void reloadUI()
    {
        if (this.gameManager.GetInventory().firstItem != this.first)
        {
            this.first = this.gameManager.GetInventory().firstItem;
            changeImage(0, (int)this.gameManager.GetInventory().firstItem);
        }
        if (this.gameManager.GetInventory().secondItem != this.second)
        {
            this.second = this.gameManager.GetInventory().secondItem;
            changeImage(1, (int)this.gameManager.GetInventory().secondItem);
        }
        if (this.gameManager.GetInventory().thirdItem != this.third)
        {
            this.third = this.gameManager.GetInventory().thirdItem;
            changeImage(2, (int)this.gameManager.GetInventory().thirdItem);
        }
    }

    public void changeImage(int image, int id)
    {
        if (id == 1)
            items[image].GetComponent<Image>().sprite = items[image].image1;
        else
            items[image].GetComponent<Image>().sprite = items[image].image2;

        var tempColor = items[image].GetComponent<Image>().color;
        tempColor.a = 1f;
        items[image].GetComponent<Image>().color = tempColor;
    }


}
