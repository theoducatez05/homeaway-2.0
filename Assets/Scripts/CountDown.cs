﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountDown : MonoBehaviour
{

    public float time = 60;
    public bool isActive = true;

    public TextMeshProUGUI textBox;

    // Start is called before the first frame update
    void Start()
    {
        this.textBox = GetComponent<TextMeshProUGUI>();
        this.textBox.text = time.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.isActive)
        {
            this.time -= Time.deltaTime;
            textBox.text = Mathf.Round(this.time).ToString();
            if (this.time < 0)
            {
                timeEnded();
                this.isActive = false;
            }
        }
    }

    void timeEnded()
    {
        Debug.Log("Lost !");
        FindObjectOfType<GameManager>().GameOver(); //tomporairy
    }
}
