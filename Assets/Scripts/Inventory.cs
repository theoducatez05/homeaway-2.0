﻿using System;

[Serializable]
public enum ThirdItem
{
    None,
    Chestnut,
    Blower
}

[Serializable]
public enum FirstItem
{
    None,
    Donkey,
    Chicken

}

[Serializable]
public enum SecondItem
{
    None,
    Beam,
    Mandolin
}

[Serializable]
public class Inventory
{
    public FirstItem firstItem = FirstItem.None;
    public SecondItem secondItem = SecondItem.None;
    public ThirdItem thirdItem = ThirdItem.None;

    public void setItem(FirstItem item)
    {
        this.firstItem = item;
    }

    public void setItem(SecondItem item)
    {
        this.secondItem = item;
    }

    public void setItem(ThirdItem item)
    {
        this.thirdItem = item;
    }
}
