﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField]
    Inventory inventory;
    public Inventory Items
    {
        get
        {
            return inventory;
        }
    }

    //Make the GameManager a singleton
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        if (SceneManager.GetActiveScene().buildIndex == 0) {
            this.inventory = new Inventory();
            this.loadNextScene();
        }
    }

    public Inventory GetInventory()
    {
        return this.inventory;
    }

    public void loadScene(int sceneNumber)
    {
        if (sceneNumber == 1 || sceneNumber == 2)
        {
            Cursor.visible = true;
        }
        else
        {
            Cursor.visible = false;
        }
        Debug.Log("Loading Scene : " + sceneNumber);
        SceneManager.LoadScene(sceneNumber);
    }

    public void realoadScene()
    {
        Debug.Log("Loading Scene : " + SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void loadNextScene()
    {
        Debug.Log("Loading Scene : " + SceneManager.GetActiveScene().buildIndex+1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void GameOver()
    {
        this.loadScene(0);
    }

    public void disableCursos()
    {
        Cursor.visible = false;
    }

}
