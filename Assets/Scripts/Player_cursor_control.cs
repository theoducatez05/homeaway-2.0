﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_cursor_control : MonoBehaviour
{
    private GameManager gameManager;

    private void Start()
    {
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPos;
    }
}
