﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1_Manager : MiniGameManager
{
    private GameManager gameManager;

    public Sprite donkeySprite_up;
    public Sprite donkeySprite_down;

    public Sprite chickenSprite_up;
    public Sprite chickenSprite_down;

    public Canvas introUi;
    private bool paused = true;

    public Canvas DeathUI;
    private bool playerDead = false;

    // Start is called before the first frame update
    void Start()
    {
        this.gameManager.disableCursos();
        Time.timeScale = 0;
        this.DeathUI.enabled = false;
    }

    void Awake()
    {
        GameObject check = GameObject.Find("__app");
        if (check == null)
        { UnityEngine.SceneManagement.SceneManager.LoadScene("_preload"); }
        this.gameManager = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (this.paused || this.playerDead)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (this.paused)
                {
                    this.paused = false;
                    this.introUi.enabled = false;
                    Time.timeScale = 1;
                }

                else if (this.playerDead)
                {
                    if (this.DeathUI.enabled)
                    {
                        gameManager.realoadScene();
                    }
                }
            }
        }
    }

    public void PlayerDead()
    {
        this.DeathUI.enabled = true;
        this.playerDead = true;
        Time.timeScale = 0;
    }

    public void gameWin()
    {
        Debug.Log("Win !");
        Time.timeScale = 0;
        //load other scene
    }

    public FirstItem getFirstItem()
    {
        return this.gameManager.GetInventory().firstItem;
    }
}
